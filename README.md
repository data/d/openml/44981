# OpenML dataset: pumadyn32nh

https://www.openml.org/d/44981

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

A realistic simulation of the dynamics of a Puma 560 robot arm. The task in this dataset is to predict the angular acceleration of one of the robot arm's links. The task is nonlinear and has high noise.

The similator got as input the angular positions, velocities and torques and other dynamic parameters of the Puma arm.

The goal was to predict the acceleration of link 6 *thetad6*.

**Attribute Description**

1. *theta[1-6]* - angular positions of links 1 to 6
2. *thetad[1-6]* - angular velocities of links 1 to 6
3. *tau[1-5]* - torques at joints 1 to 5
4. *dm[1-5]* - change in mass of links 1 to 5
5. *da[1-5]* - change in length of links 1 to 5
6. *db[1-5]* - change in viscous friction of links 1 - 5
7. *thetadd6* - acceleration of link 6, target feature

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44981) of an [OpenML dataset](https://www.openml.org/d/44981). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44981/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44981/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44981/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

